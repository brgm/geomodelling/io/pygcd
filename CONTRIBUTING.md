Contributing
============

This article documents how to contribute improvements to ~~virtually any~~this package.

Setup
-----

Before you begin, perform initial setup:

  1. [Register for a GitLab](https://gitlab.brgm.fr) account.
  1. Follow the [README](README.md#installation) to create a local clone:

```bash
   git clone my-awsome-package.git
```

  3. (optional) Install pre-commit [hooks](.pre-commit-config.yaml):

```bash
   pre-commit install
```

Workflow
--------

This module development uses a simple branchy workflow based on topic branches.
This corresponds to the simple *GitHub Flow* also known as *Short Lived Feature Branches Flow*.
Our collaboration workflow consists of three main steps:

  1. Local Development
     * [Update](#update)
     * [Create a Topic](#create-a-topic)
     * [Commit Messages](#commit-messages)
  2. Code Review
     * [Share a Topic](#share-a-topic)
     * [Revise a Topic](#revise-a-topic)
  3. Integrate Changes
     * [Merge a Topic](#merge-a-topic)
     * [Delete a Topic](#delete-a-topic)

Update
------

Update your local `main` branch:

```bash
   git fetch --all
   git checkout main
   git pull
```

Create a Topic
--------------

All new work must be committed on topic branches. Name topics like you might name functions: concise but precise.
A reader should have a general idea of the feature or fix to be developed given just the branch name.

To start a new topic branch:

```bash
   git fetch upstream
```

For new development, start the topic from `origin/main`:

```bash
   git checkout -b my-topic origin/master
```

A good practice is to create a *branch* along with the *Merge Request* directly from a GitLab [issue](https://gitlab.brgm.fr/brgm/geomodelling/public/pygcd/issues).
It allows other contributors to track the topic origin, related discussions and status.

(*You may visit the* Pro Git: Basic Branching *resource in [Git Help] for
further information on working with branches.*)

Edit files and create commits (repeat as needed). Add a prefix to your commit
message (see below).

```bash
   edit file1 file2 file3
```

```bash
   git add file1 file2 file3
   git commit
```

(*You may visit the* Pro Git: Recording Changes *resource in [Git Help] for further information on making changes and committing snapshots.*)

Commit Messages
---------------

Write your commit messages preferably using the standard prefixes for commit messages:
-   **CI:**      continuous integration pipelines
-   **BUG:**     fix for runtime crash or incorrect result
-   **DOC:**     documentation change
-   **ENH:**     new functionality
-   **REF:**     code refactoring
-   **COMP:**    build system change
-   **CI:**      GitlabCI related changes
-   **TEST:**    test changes
-   **STYLE:**   coding style
-   **WIP:**     work in progress (not ready for merge)

The first line of the commit message should preferably be 72 characters or less.

Follow the first line commit summary with an empty line, then a detailed description in one or more paragraphs.
The body of the message should clearly describe the motivation of the commit (**what**, **why**, and **how**). In order to ease the task of reviewing commits, the message body should follow the following guidelines:

  1. Leave a blank line between the subject and the body.
  This helps `git log` and `git rebase` work nicely, and allows to smooth
  generation of release notes.
  1. Try to keep the subject line below 72 characters, ideally 50.
  1. Capitalize the subject line.
  1. Do not end the subject line with a period.
  1. Use the imperative mood in the subject line (e.g. `BUG: Fix spacing
  not being considered.`).
  1. Wrap the body at 80 characters.
  1. Use semantic line feeds to separate different ideas, which improves the
  readability.
  1. Be concise, but honor the change: if significant alternative solutions
  were available, explain why they were discarded.
  1. If the commit refers to a topic discussed in GitLab. If the commit closes an
  issue, use the [GitLab issue closing keywords](https://docs.GitLab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).

Keep in mind that the significant time is invested in reviewing commits and *merge requests*, so following these guidelines will greatly help the people doing reviews.

These guidelines are largely inspired by Chris Beam's
[How to Write a Commit Message](https://chris.beams.io/posts/git-commit/)
post.


Share a Topic
-------------

When a topic is ready for review and possible inclusion, share it by pushing to GitLab and opening a *merge request* on the upstream repository.

Checkout the topic if it is not your current branch:

```bash
   git checkout my-topic
```

Push commits in your topic branch for review by the community:

```bash
   git push --force
```

Revise a Topic
--------------

Usually, a topic goes through several revisions in the review process. Once a topic is approved during GitLab review, proceed to the [next step](#merge-a-topic).

Checkout the topic if it is not your current branch:

```bash
   git checkout my-topic
```

To revise the most recent commit on the topic edit files and add changesnormally and then amend the commit:

```bash
   git commit --amend
```

(*You may visit the* Pro Git: Changing the Last Commit *resource in [Git Help] for further information on revising and rewriting your commit history.*)

To revise commits further back on the topic, say the `3`rd commit back:

```bash
   git rebase -i HEAD~3
```

(*Substitute the correct number of commits back, as low as `1`.*)

Follow Git's interactive instructions.

Return to the [Share a Topic](#share-a-topic) step to share the revised topic.

(*You may visit the* Pro Git: Changing Multiple Commits *resource in [Git Help] for further information on changing multiple commits -i.e. not only the last one, but further back in your history-, and the* Pro Git: Rebasing *resource on taking all the changes that were committed on one branch and replaying them on another one.*)

Merge a Topic
-------------

**Only authorized developers with GitLab merge permissions execute this step.**

After a topic has been reviewed and approved in GitLab, maintainers will merge it into the upstream repository via the GitLab user interface.


Delete a Topic
--------------

After a topic has been merged upstream, delete your local branch for the topic.

Checkout and update the `develop` branch:

```bash
   git checkout develop
   git pullall
```

Delete the local topic branch:

```bash
   git branch -d my-topic
```

The `branch -d` command works only when the topic branch has been correctly merged. Use `-D` instead of `-d` to force the deletion of an unmerged topic branch (*warning*: you could lose commits).

[Git Help]: Documentation/GitHelp.md
