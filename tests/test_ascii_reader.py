# -*- coding: utf-8 -*-
import pygcd


def test_reading(samples):
    for filename in samples:
        blocks = pygcd.read(filename)
        assert blocks
