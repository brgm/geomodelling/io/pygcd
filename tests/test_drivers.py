# -*- coding: utf-8 -*-
from logging import INFO, WARNING, log

import pygcd


def test_reading(samples):
    for filename in samples:
        blocks = pygcd.read(filename)
        assert blocks
        for name, wrapper in pygcd.Drivers.items():
            try:
                log(INFO, f"Testing {name} driver")
                result = wrapper(blocks)
                assert result is not None
            except ImportError:
                log(WARNING, f"{name} could not be found ! abort testing.")
