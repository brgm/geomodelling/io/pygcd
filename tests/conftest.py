# -*- coding: utf-8 -*-
from pathlib import Path

import pytest


@pytest.fixture
def samples():
    return list(Path(__file__).parent.glob("samples/*.so"))
