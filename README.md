# PyGCD

Read & convert GOCAD ASCII exports.

> It is based on the documentation harvested on the web [here](http://paulbourke.net/dataformats/gocad/gocad.pdf) and [there](https://scholar.harvard.edu/files/plesch/files/gocadinahurry.pdf), recycling some of the [coseis](https://github.com/elygeo/coseis/blob/master/cst/gocad.py) routines.

`pygcd` can read GOCAD objects:
- [x] TSurf
- [x] TSolid
- [x] Vset
- [x] PLine
- [x] Well
- [ ] Voxet
- [ ] GSurf
- [ ] SGrid

and knows how to serve them as third-party objects:
- [x] Python [objects](#objects)
- [x] [geopandas](https://geopandas.org/).`GeoDataFrame` *(requires `geopandas`)*
- [x] [pyvista](https://docs.pyvista.org/).`DataObject` *(requires `pyvista`)*
- [x] [lasio](https://lasio.readthedocs.io/en/latest/) log files *(requires `lasio`)*

## Summary

- [Installation](#installation)
- [Quickstart](#quickstart)
    - [SIG converting](#sig-converting)
    - [3D rendering](#3d-rendering)
    - [Well curves](#las-export)
- [Documentation](#whats-inside)
- [What's next](#todo-list)
- [Contributing](#contributing)

## Installation

Latest release can be installed using:
```sh
pip install pygcd -i https://nexus.brgm.fr/repository/pypi-all/simple
```
Additional dependencies (`pyvista`, `geopandas`, ...) are required for serving the output into third-party formats.
```sh
pip install pygcd[drivers] -i https://nexus.brgm.fr/repository/pypi-all/simple
```

## Quickstart

### Basic reading

```py
import pygcd

blocks = pygcd.read(filename)
```
with `blocks` a list of `Objects` with the following structure:
```py
(
    name: str,
    geometry: Enum
    version: str
    fields: dict

    [...] # extra based on geometry
)
```

### SIG converting

```py
import pygcd

df = pygcd.read(filename, wrapper='geopandas')
df = df[df.geometry!=None]  # remove unsupported features

from pathlib import Path
outfile = Path(filename).with_suffix('.geojson').as_posix()
df.to_file(outfile)  # write everything "as is"

# or filter by geometry:
df['geom_type'] = [g.type for g in df.geometry]
for label, group in df.groupby('geom_type'):
    outfile = Path(filename).with_suffix(f'.{label}.shp').as_posix()
    group.to_file(outfile)
```

### 3D rendering

```py
import pygcd
vtm = pygcd.read(filename, wrapper='pyvista')
vtm.plot(multi_colors=True)
```

### LAS export

```py
import pygcd
las = pygcd.read(filename, geometries=["Well"], wrapper='las')
for l in las:
   file = l.well.WELL.value + ".las"
   l.write(file, version=2)
```

## What's inside ?

`pygcd` offers 3 reading levels for ASCII files:
- `def find(file:str) -> list[str]`: splits file content into single object text chunks
- `def load(file:str) -> Dataset[Chunk]`: read the header only *with global attributes (name, geometry type, version, etc...)*
- `def read(file:str) -> Dataset[Object]`: fully decode the objects *with geometry specific attributes (points, cells, curves, etc...)*

it provides 2 types of objects:
- `class Dataset(list)`: a list with file reference
    - `filename (str)`: the input file
    - convenient attributes getters *(get block by name, select gemetry, ...)*
    - `to(self, wrapper:str) -> Any`: method to cast objects into the supported wrappers *('pyvista', 'geopandas', ...)*
- `class Layer(object)`: an abstract generic class for GOCAD objects holding metadata:
    - `name (str)`: the object __name__ attribute
    - `geometry (Enum)`: the GOCAD object type name
    - `version (str)`: the GOCAD version of the geometry *(unused feature)*
    - `fields (dict)`: the mapping of GOCAD object header properties

### Layers

- `class Chunk(Layer)`: a specialization for ascii text chunks
    - `chunk (str)`: ascii block containing the object serialization
    - `decode(self) -> Object`: method to decode the geometry
- `class Object(Layer)`: an abstract class for geometry-specific content
    - `from_chunk(block:str)->Object`: method to parse ascii chunk

### Objects

- `class Mesh(Object)`: for __VSet__, __PLine__, __TSurf__ and __TSolid__
    - `points (list)`: n-by-3 points coordinates
    - `cells (list[tuple[int]])`: list of point indices
    - `point_data (dict)`: key-mapped point properties
    - `cell_data (dict)`: key-mapped cell properties
- `class Well(Object)`: for __Well__
    - `collar (tuple[float])`: 3 points coordinates (x,y,z)
    - `path (list[tuple[float]])`: n-by-4 trajectory coordinates (x, y, z, depth)
    - `markers (list[namedtuple])`: list of markers (`name`, `zm`:relative z + other extra fields)
    - `zones (list[namedtuple])`: list of zones/zones/trunks (`name`,
    `zfrom`:relative z, `zto`:relative z + other extra fields)
    - `curves (list[namedtuple])`: list of curves (i.e. diagraphies) along the path (`name`, `zm`:relative z, `values`:array + other extra fields)
- `class Grid(Object)`: for __Voxet__, __GSurf__, __SGrid__
    - __WIP__: `raise NotImpentedError()`

## TODO list
- [ ] Read geometries
    - [x] TSurf
    - [x] TSolid
    - [x] Vset
    - [x] PLine
    - [x] Well
    - [ ] Voxet
    - [ ] GSurf
    - [ ] SGrid
- [ ] Read data properties
    - [x] Atom/Vertex properties
    - [x] Well properties
    - [x] Well curves
    - [ ] Region properties *(it is unclear how to use these ATM ...)*
- [ ] Add dependency-free (or pure-python only) converters
    - [x] GML/GeoJSON SIG formats
    - [x] Generic mesh formats *(using [meshio](https://github.com/nschloe/meshio)?)*
- [ ] Provide a GDAL driver *for SIG native integration*
- [ ] Provide a format conversion REST-API
    - [ ] GML/ GeoJSON
- [ ] Release publicly to PyPI


**Feel free to [contribute](#contributing) or open an [issue](./-/issues) to request a feature**

## Contributing
Any help with this tool is welcomed, see [CONTRIBUTING.md](CONTRIBUTING.md) for "guidelines" *(strong word for a merely step-by-step how-to... feel free to contribute as you whishes)*.
